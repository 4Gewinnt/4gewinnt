Projektdokumentation

zum Testen bitte zuerst gulp default ausführen (dazu alles notwendige installieren)
und dann das index.html im dist-Ordner öffnen, am besten mit Google Chrome :)


Zoe

Ich habe f�r unser Spiel das js-Model hergenommen, das ich in einer anderen �bung selbst erstellt habe.
Dieses musste aber lediglich zu zweit bedienbar sein, man konnte nicht gegen den Computer spielen
und auch keine pers�nlichen Einstellungen machen.
Gemeinsam mit Marlene habe ich das Model so umfunktioniert, dass pers�nliche Einstellungen zu Beginn
und jederzeit w�hrend dem Spiel m�glich sind. Au�erdem kann man jetzt gegen den Computer spielen (siehe Marlene!)
Die genaue Funktionsweise des Models ist im Code kommentiert.

Auch die View habe ich grunds�tzlich in einer anderen �bung erstellt, jedoch war diese auf ein wieder anderes Model
zugeschnitten. Ich habe die View wiederum an das von uns modifizierte Model angpasst, was zu einigen Problemen f�hrte,
da unser Model viel mehr Eventtrigger wirft, als f�r die die einzelnen Views registriert sind. Au�erdem war sie nicht
daf�r gedacht, dass es auch einen Startbildschirm ohne dem Spiel selbst gibt. Anstatt die einzelnen Views gleich zu
erzeugen musste das jetzt erst nach dem Klick auf "Start" passieren, was wiederum zu einigen Komplikationen und Wirrwarr
im Code f�hrte. Mit jeder Menge Zeitaufwand und viel Probieren hab ich diese ganzen Kleinigkeiten letztendlich lösen können.

Au�erdem haben wir eine sass-Datei erstellt, um den Umgang mit sass zu �ben. Dies haben wir alle gemeinsam
gemacht.


Kathi

Ich habe am Anfang das Bitbucket Repository erstellt um so das Projekt besser verwalten zu 
k�nnen. Auch habe ichmir Sass etwas n�her angeschaut. Zuerst habe ich mir einige Tutorials 
angeschaut wie Sass verwendet werden kann. Danach habe ich Sass installiert und wir haben 
zusammen eine erste .scss Datei angelegt. Zun�chst wurden wichtige Variablen, wie Farben 
oder Gr��en definiert und sie f�r die entsprechenden Elemente verwendet. Zuerst wurden die 
.scss Dateien mit dem Command 'sass --watch style.scss:style.css' in eine .css Datei umgewandelt. 
Danach haben wir uns entschlossen dem Projekt eine GulpPipeline hinzuzuf�gen um Prozesse zu 
erleichtern. Angefangen haben wir mit dem html und css task. Danach wurde ein sass task eingebunden 
('gulp-sass'). Danach ist mir aufgefallen, dass f�r den Hintergrund Bilder in der .css Datei referenziert sind. 
Deswegen wurde ein Task in die gulpfile.js inkludiert um .jpg und .svg in den dist Ordner zu �bermitteln 
('gulp-image' und 'gulp-svgo'). Alle Tasks wurden in einen default Task geschrieben und so konnte die Anwendung 
im Browser gestartet werden.


Marlene

Mein besonderer Wunsch an die Gruppe war es, mit Git zu arbeiten. Zuvor war mir dies leider noch nicht m�glich. 
Anfangs hatte ich ehrlich gesagt schon ein paar Probleme mich zurecht zu finden - aber Kathi und Zoe unterst�tzten 
mich tatkr�ftig. :)

Meine Aufgaben waren es, diverse Dinge in JS mit Zoe zu programmieren (Gegen den Computer 
spielen, aufsetzen Spiellogik, Stylen der Eingaben, Buttons,...). Sprich M�dchen f�r alles, was gerade so 
angefallen ist. Die CSS haben wir letztendlich auch gemeinsam in SASS umgeschrieben. Genauere Dokumentationen 
sind im Code selbst ersichtlich.