let gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    gulpif = require('gulp-if'),
    connect = require('gulp-connect'),
    clean = require('gulp-clean'),
    gulpSequence = require('gulp-sequence'),
    cssnano = require('gulp-cssnano'),
    image = require('gulp-image'),
    sass = require('gulp-sass'),
    svgo = require('gulp-svgo');

let env = 'prod';
let path = {
    srcSCSS: '/scss/*.scss',
};

gulp.task('sass', function () {
    return gulp.src('./scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['last 20 versions'],
            cascade: false
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(connect.reload());
});

gulp.task('css', () => {
    return gulp.src('./vendor/**.css')
        .pipe(sourcemaps.init())
        .pipe(autoprefixer({
            browsers: ['last 20 versions'],
            cascade: false
        }))
        .pipe(cssnano())
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./dist/vendor'))
        .pipe(connect.reload());
});




gulp.task('svg', () => {

    return gulp.src('./assets/*.svg')
        .pipe(svgo())
        .pipe(gulp.dest('./dist/assets'))
        .pipe(connect.reload());
});

gulp.task('image', function () {
    return gulp.src('./assets/*.jpg')
        .pipe(image())
        .pipe(gulp.dest('./dist/assets'))
        .pipe(connect.reload())
});


gulp.task('watch:css', () => {
    gulp.watch('*.css', ['css']);
});

gulp.task('watch:html', () => {
    gulp.watch('*.html', ['moveHTML']);
});

gulp.task('moveHTML', () => {
    return gulp.src('*.html')
        .pipe(gulp.dest('./dist/'))
        .pipe(connect.reload())
});

gulp.task('moveJS', ()=>{
    return gulp.src('./js/**.js')
        .pipe(gulp.dest('./dist/js'))
        .pipe(connect.reload())
});

gulp.task('moveVendor', ()=>{
    return gulp.src('./vendor/**.js')
        .pipe(gulp.dest('./dist/vendor'))
        .pipe(connect.reload())
});

gulp.task('connect', () =>{
    return connect.server({
        root: './dist',
        port: 1335,
        livereload: true
    });
});

gulp.task('clean', () => {
    return gulp.src('./dist')
        .pipe(clean())
});

gulp.task('default', (cb) => {
    gulpSequence('clean',
        'connect',
        'moveJS',
        'moveVendor',
        ['moveHTML', 'css', 'image', 'svg', 'sass'],
        ['watch:html'],
        cb
    )
});