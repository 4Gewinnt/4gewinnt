lib = window.lib || {};

(function () {
    class  SimpleView{
        /**
         * param model
         * param dom where to append output
         * constructor
         * return nothing
         */
        constructor(model, dom){
            // get model and dom
            this._model = model;
            this._dom = dom;

            // add clickhandler
            this._clickhandler();

            // add other event listener
            $(model).on('start', this._init.bind(this));
            $(model).on('computer', this._computer.bind(this));
            $(model).on('change', this._render.bind(this));
            $(model).on('gameOver', this._gameOver.bind(this, true));
            $(model).on('playerWon', this._gameOver.bind(this, false));
        }

        _computer(){
            setTimeout(this._computer2.bind(this), 2000);
        }

        _computer2(){
            this._model._computerSetToken();
        }

        _init(){
            console.log("in start trigger");
            $('#startDisplay').hide();
            this._show();
            this._render.bind(this);
        }

        /**
         * no params
         * output of game
         * return nothing
         */
        _render(){
            // remove current output
            this._dom.empty();

            // output of current player
            this._dom.append('<p class="text">' + this._model.getCurrentPlayerName() + ' ist dran.</p>');

            // get string
            let string = this._model.toString();

            // get column headers
            let numbers = "";
            for(let i=1; i<=this._model.width; i++){
                numbers += i;
            }

            // append column headers, string and restart button to dom
            this._dom.append('<p class="field">'+numbers+'<br/>'+string+'</p>');
            this._dom.append(
                '<button class="button" id="restart">Neu</button>' +
                '<button class="button" id="settings">Einstellungen</button>'
            );
        }

        /**
         * param text to append at dom
         * output of game when game over
         * return nothing
         */
        _gameOver(gameOver){
            // remove current output
            this._dom.empty();

            // get string and append it to dom
            let string = this._model.toString();
            this._dom.append('<p class="field">'+string+'</p>');

            // append message and restart button to dom
            if(gameOver)
                this._dom.append(
                    '<p class="text">GAME OVER - The playfield is full!</p>' // TODO
                    + '<button class="button" id="restart">New game</button>'
                    + '<button class="button" id="settings">Settings</button>'
                );
            else
                this._dom.append(
                    '<p class="text">'+this._model.getCurrentPlayerName()+' wins!</p>' // TODO
                    + '<button class="button" id="restart">New</button>'
                    + '<button class="button" id="settings">Settings</button>'
                );
        }

        /**
         * no parameters
         * adds event listener to keyup and click on restart button
         * return nothing
         */
        _clickhandler(){
            // event listener to keyup event
            document.addEventListener("keyup", (evt) => {
                // convert the keyCode of the pressed key to the number for which it stands
                let index = Number(String.fromCharCode(evt.keyCode)) - 1;
                // ask model if insert of token is possible at pressed index
                if(this._model.isInsertTokenPossibleAt(index))
                    // if yes - insert token at pressed index
                    this._model.insertTokenAt(index);
            });

            // event listener to click event on restart button
            this._dom.on("click", "#restart", () => {
                // reload game
                this._model.init(this._model.nameP1, this._model.nameP2, this._model.player1,
                    this._model.player2, this._model.width, this._model.height, this._model.againstComputer);

            });

            this._dom.on("click", "#settings", () => {
                // adjust settings
                this._model.adjustSettings();
                this._hide();
            });
        }

        _hide(){
            this._dom.hide();
        }

        _show(){
            this._dom.fadeIn();
        }
    }

    class StartDisplay{
        /**
         * param model
         * param dom where to append output
         * constructor
         * return nothing
         */
        constructor(dom, model){
            // get model and dom
            this._dom = dom;
            this._model = model;

            // add clickhandler
            this._clickhandler();

            // add other event listener
            if(this._model !== null)
                $(this._model).on('settings', this._render.bind(this));
        }

        /**
         * no params
         * output of game
         * return nothing
         */
        _render(){
            console.log("neu laden");
            this._dom.fadeIn();
            let html = "<h1>Connect Four</h1>"+
                "<div class='playerOptions'>"+
                "<p class='text'>Choose your personal settings for the game.</p><hr/>" +
                "<div class='half'><label for='p1name'>Name of player 1: </label><br/>"+
                "<input type='text' id='p1name' size='20' value='"+this._model.nameP1+"'/></div>"+
                "<div class='half'><label for='p2name'>Name of player 2: </label><br/>"+
                "<input type='text' id='p2name' size='20' value='"+this._model.nameP2+"'/></div><br/><br/><hr/>"+

                "<label for='againstComputer'>Playing against the computer: </label>"+
                "<select id='againstComputer' >" +
                "<option value='no'>No</option>"+
                "<option value='yes'>Yes</option>"+
                "</select><br/><hr/>" +

                "<div class='half'><label for='cols'>Columns: </label>"+
                "<select id='cols' >" +
                "<option value='4'>4</option>"+
                "<option value='5'>5</option>"+
                "<option value='6'>6</option>"+
                "<option value='7' selected>7</option>"+
                "<option value='8'>8</option>"+
                "<option value='9'>9</option>"+
                "</select> </div>"+

                "<div class='half'><label for='rows'>Rows: </label>"+
                "<select id='rows' >" +
                "<option value='4'>4</option>"+
                "<option value='5'>5</option>"+
                "<option value='6' selected>6</option>"+
                "<option value='7'>7</option>"+
                "<option value='8'>8</option>"+
                "<option value='9'>9</option>"+
                "</select></div> <br/>"+

                "<button class='button' id='start' style='margin-left: 22%'><span>Start game </span></button>" +
                "</div>"
            ;
            this._dom.append(html);
        }

        _clickhandler(){
            // event listener to click event on start button
            this._dom.on("click", "#start", () => {
                // read options
                let p1name = $('#p1name').val();
                let p2name = $('#p2name').val();

                let cols = $('#cols').val();
                let rows = $('#rows').val();

                let againstComputerValue = false;
                if($('#againstComputer').val() == "yes"){
                    againstComputerValue = true;
                    p2name = "Computer"
                }

                this._dom.empty();
                this._dom.hide();

                this._model.setNames(p1name, p2name);
                this._model.setField(cols, rows);
                this._model.setAgainstComputer(againstComputerValue);
                this._model.init(this._model.nameP1, this._model.nameP2, this._model.player1, this._model.player2,  this._model.width, this._model.height, this._model.againstComputer);
            });
        }
    }

    lib.SimpleView = SimpleView;
    lib.StartDisplay = StartDisplay;
}());
