lib = window.lib || {};

(function () {
    class Controls{
        /**
         * param model
         * param dom where to append output
         * constructor
         * return nothing
         */
        constructor(model, dom){
            // get model and dom
            this._model = model;
            this._dom = dom;

            // add event listener
            $(model).on('initComplete', this.init.bind(this));
            $(model).on('change', this._render.bind(this));
            $(model).on('gameOver', this._onBoardFull.bind(this));
            $(model).on('playerWon', this._onGameOver.bind(this));
        }

        /**
         * no params
         * set helper variable begin to true and render view
         * return nothing
         */
        init(){
            console.log('init controls...');
            $('#fancyGame').fadeIn();

            $('#startDisplay').hide();

            this._render();
        }

        /**
         * no params
         * render text for current player and his token
         * return nothing
         */
        _render(){
            // get current player
            let currentPlayer;
            if(this._model.currentPlayer) currentPlayer = 1;
            else currentPlayer = 2;

            // output
            let html = '';
            html += '<span class="text">Current player: '+this._model.getCurrentPlayerName()+'</span>';
            html += '<span class="controls ball player'+currentPlayer+'">' +
                '<div class="innerBall player'+currentPlayer+'"></div></span>';
            this._dom.html(html);

            // check if full
            if(this._checkIfFull()) this._onBoardFull();


            console.log(this._model.toString());
        }

        /**
         * no params
         * checks if insert token would be possible at any column
         * return true or false
         */
        _checkIfFull(){
            for(let cols=0; cols<this._model.width; cols++){
                if(this._model.isInsertTokenPossibleAt(cols))
                    return false;
            }
            return true;
        }

        /**
         * no params
         * show error message that board is full
         * return nothing
         */
        _onBoardFull(){
            this._dom.html(
                "<p class='text' style='text-align: center'>GAME OVER" +
                "<br/><br/>No space left! Please restart the game.</p>");
        }

        /**
         * no params
         * show message which player won
         * return nothing
         */
        _onGameOver(){
            let dom = this._dom;
            let model = this._model;
            let timeout = window.setTimeout(function () {
                if(model.getCurrentPlayerName() == "Computer"){
                    dom.html("<p class='text' style='text-align: center'>GAME OVER" +
                        "<br/><br/>The "+model.getCurrentPlayerName()+" wins!</p>");
                } else {
                    dom.html("<p class='text' style='text-align: center'>CONGRATULATIONS" +
                        "<br/><br/>"+model.getCurrentPlayerName()+" wins!</p>");
                }
                window.clearTimeout(timeout);
            }, 1500);
        }
    }

    class Restart{
        /**
         * param model
         * param dom where to append output
         * constructor
         * return nothing
         */
        constructor(model, dom){
            // get model and dom
            this._model = model;
            this._dom = dom;

            // add event listener
            $(this._model).on("initComplete", this.init.bind(this));
            $(this._model).on("playerWon", this._onGameOver.bind(this));
        }

        /**
         * no params
         * removes event listener, creates restart button
         * return nothing
         */
        init(){
            console.log('init restart...');
            Restart._removeEventListener();
            this._dom.html("<button class='button' id='restart' style='margin-right: 15px'><span>Restart</span></button>" +
                "<button class='button' id='settings' style='margin-left: 15px'><span>Settings</span></button>");

            // add clickhandler to restart button
            $('#restart').on("click", (e) => {
                // remove existing alert window
                $('.popup-background').remove();
                // create new alert window
                this._dom.append(
                    "<div class='popup-background' style='display: none'>" +
                    "<div class='popup'>" +
                    "<p class='text' style='text-align: center'>" +
                    "Are you sure you want to restart the game?</p>" +
                    "<div class='button-container'> " +
                    "<button id='restart2' class='button' style='margin-right: 15px'><span>Restart</span></button>"+
                    "<button id='cancel' class='button' style='margin-left: 15px'><span>Cancel</span></button>"+
                    "</div>"+
                    "</div>" +
                    "</div>"
                );
                $('.popup-background').fadeIn();

                // add clickhandler
                this._clickhandler();
            });

            // add clickhandler to settings button
            $('#settings').on("click", (e) => {
                // remove existing alert window
                $('.popup-background').remove();
                // create new alert window
                if(this._model.nameP2 == "Computer"){
                    this._model.nameP2 = "Player2";
                }

                this._dom.append(
                    "<div class='popup-background' style='display: none'>" +
                    "<div class='popup'>" +
                    "<p class='text' style='text-align: center'>" +
                    "Choose personal settings for your game.<br/>When you click on Change the game will <br/> be restarted.</p>" +
                    "<hr/>"+

                    "<div class='half'><label for='nameP1'>Name of player 1: </label><br/>"+
                    "<input type='text' id='nameP1' size='20' value='"+this._model.nameP1+"'/></div>"+
                    "<div class='half'><label for='nameP2'>Name of player 2: </label><br/>"+
                    "<input type='text' id='nameP2' size='20' value='"+this._model.nameP2+"'/></div><br/><br/><hr/>"+

                    "<label for='againstComputer'>Playing against the computer: </label>"+
                    "<select id='againstComputer' >" +
                    "<option value='no'>No</option>"+
                    "<option value='yes'>Yes</option>"+
                    "</select><br/><hr/>" +

                    "<div class='half'><label for='numCols'>Columns: </label>"+
                    "<select id='numCols' >" +
                    "<option value='4'>4</option>"+
                    "<option value='5'>5</option>"+
                    "<option value='6'>6</option>"+
                    "<option value='7' selected>7</option>"+
                    "<option value='8'>8</option>"+
                    "<option value='9'>9</option>"+
                    "</select> </div>"+

                    "<div class='half'><label for='numRows'>Rows: </label>"+
                    "<select id='numRows' >" +
                    "<option value='4'>4</option>"+
                    "<option value='5'>5</option>"+
                    "<option value='6' selected>6</option>"+
                    "<option value='7'>7</option>"+
                    "<option value='8'>8</option>"+
                    "<option value='9'>9</option>"+
                    "</select></div> <br/>"+

                    "<div class='button-container'> " +
                    "<button type='submit' id='settings2' class='button' style='margin-right: 15px'><span>Change</span></button>"+
                    "<button id='cancel' class='button' style='margin-left: 15px'><span>Cancel</span></button>"+
                    "</div>"+
                    "</div>" +
                    "</div>"
                );
                $('.popup-background').fadeIn();

                // add clickhandler
                this._clickhandler();
            });
            this._clickhandler();
        }

        /**
         * no params
         * removes all clickhandler
         * return nothing
         */
        static _removeEventListener(){
            $('#cancel').off('click');
            $('#restart').off('click');
            $('#restart2').off('click');
        }

        /**
         * no params
         * creates alert window with message which player won
         * return nothing
         */
        _onGameOver(){
            // remove existing alert window
            $('.popup-background').remove();

            let clickhandler = this;
            let dom = this._dom;
            let currentPlayer = this._model.getCurrentPlayerName();
            let timeout = window.setTimeout(function () {
                if(currentPlayer === "Computer"){
                    dom.append(
                        "<div class='popup-background' style='display: none'>" +
                        "<div class='popup'>" +
                        "<p class='text' style='text-align: center'>GAME OVER" +
                        "<br/><br/>The "+currentPlayer+" wins!</p>" +
                        "<div class='button-container'> " +
                        "<button id='restart2' class='button' style='margin-right: 15px'><span>Restart</span></button>"+
                        "<button id='cancel' class='button' style='margin-left: 15px'><span>Review Game</span></button>"+
                        "</div>"+
                        "</div>" +
                        "</div>"
                    );
                } else {
                    dom.append(
                        "<div class='popup-background' style='display: none'>" +
                        "<div class='popup'>" +
                        "<p class='text' style='text-align: center'>CONGRATULATIONS" +
                        "<br/><br/>"+currentPlayer+" wins!</p>" +
                        "<div class='button-container'> " +
                        "<button id='restart2' class='button' style='margin-right: 15px'><span>Restart</span></button>"+
                        "<button id='cancel' class='button' style='margin-left: 15px'><span>Review Game</span></button>"+
                        "</div>"+
                        "</div>" +
                        "</div>"
                    );
                }

                $('.popup-background').fadeIn();
                clickhandler._clickhandler();
                window.clearTimeout(timeout);
            }, 1500);

            // add clickhandler
            this._clickhandler();
        }

        /**
         * no params
         * adds clickhandler to restart and cancel button
         * return nothing
         */
        _clickhandler(){
            // clickhandler on restart
            $('#restart2').on("click", (e) => {
                // init model
                this._model.init();
                $('.popup-background').fadeOut();
            });

            // clickhandler on cancel
            $('#cancel').on("click", (e) => {
                $('.popup-background').fadeOut();
            });

            // clickhandler on settings
            $('#settings2').on("click", (e) => {
                let nameP1 = $('#nameP1').val();
                let nameP2 = $('#nameP2').val();

                let againstComputerValue = false;
                if($('#againstComputer').val() == "yes"){
                    againstComputerValue = true;
                    nameP2 = "Computer"
                }

                let rows = $('#numRows').val();
                let cols = $('#numCols').val();

                if(nameP1 !== "") this._model.nameP1 = nameP1;
                if(nameP2 !== "") this._model.nameP2 = nameP2;
                this._model.againstComputer = againstComputerValue;
                this._model.width = cols;
                this._model.height = rows;

                this._model.init();
                $('.popup-background').fadeOut();
            });
        }
    }

    class Board{
        /**
        * param model
        * param dom where to append output
        * constructor
        * return nothing
        */
        constructor(model, dom){
            // get model and dom
            this._model = model;
            this._dom = dom;

            $(model).on('initComplete', this.init.bind(this));
            $(model).on('computersTurn', this._computer.bind(this));
            let helperThis = this;
            $(model).on('tokenInsertedByComputer', function (event, col) {
                helperThis._insertToken(col);
            });
            $(model).on('playerWon', this._onGameOver.bind(this));
            $(model).on('gameOver', this._onGameOver.bind(this));
        }

        _computer(){
            Board._removeEventListener();
            $('#fancyGame .board .cell').removeClass('hover');
            setTimeout(this._computer2.bind(this), 2000);
        }

        _computer2(){
            this._model._computerSetToken();
            this._clickhandler();
        }

        /**
         * no params
         * removes eventlistener, renders the board and adds eventlistener again
         * return nothing
         */
        init(){
            console.log('init board...');
            Board._removeEventListener();
            this._renderBoard();
        }

        /**
         * no params
         * removes eventlistener
         * return nothing
         */
        static _removeEventListener(){
            $(window).off("keypress.connectFour");
            $('#fancyGame .board .row>div').off("click");
            $('#fancyGame .board .row>div').off("mousedown");
            $('#fancyGame .board .row>div').off("mouseup");
            $('#fancyGame .board .row>div').off("mouseenter");
            $('#fancyGame .board .row>div').off("mouseleave");
        }

        /**
         * no params
         * adds clickhandler to board
         * return nothing
         */
        _clickhandler(){
            // add clickhandler to column
            $('#fancyGame .board .row>div').on("mouseup", (e) => {
                for(let i=0; i<this._model.width; i++){
                    if($(e.currentTarget).hasClass(i)){
                        this._insertToken(i);
                    }
                }
            });
            // add class hover on mouseenter
            $('#fancyGame .board .row>div').on("mouseenter", (e) => {
                // get number of current column
                let col;
                for(let i=0; i<this._model.width; i++){
                    if($(e.currentTarget).hasClass(i)){
                        col = i;
                        break;
                    }
                }
                // check if column is not full yet
                if(this._model.isInsertTokenPossibleAt(col)){
                    let divs = $('#fancyGame .board .cell');
                    // add class hover to each div of the column
                    for(let i=0; i<divs.length; i++){
                        if($(divs[i]).hasClass(col)){
                            $(divs[i]).addClass('hover');
                        }
                    }
                    // show token over column
                    for(let i=0; i<divs.length; i++){
                        if($(divs[i]).hasClass(col)){
                            let player;
                            if(this._model.getCurrentPlayer()) player = 1;
                            else player = 2;
                            $('.row:first-child').append(
                                '<div class="toDrop ball player'+player+' '+col+'">' +
                                '<div class="innerBall player'+player+'"></div></div>'
                            );
                            $('.row:first-child .toDrop').css('left',  col*60+10);
                            this._tokenInteraction();
                            break;
                        }
                    }
                }
                this._tokenInteraction();
            });

            // remove class hover on mouseleave
            $('#fancyGame .board .row>div').on("mouseleave", (e) => {
                let divs = $('#fancyGame .board .cell');
                for(let i=0; i<divs.length; i++){
                    $(divs[i]).removeClass('hover');
                }
                $('.row:first-child .toDrop').remove();
            });
        }

        /**
         * no params
         * adds eventlistener for hover over and click on tokens
         * return nothing
         */
        _tokenInteraction(){
            $('.row:first-child>.toDrop').on("hover", (e) => {
                $('.row:first-child .toDrop').addClass("hover");
            });
            $('.row:first-child>.toDrop').on("mousedown", (e) => {
                $('.row:first-child .toDrop').css('filter', 'brightness(0.7)');
            });
        }

        /**
         * no params
         * is called on game over
         * renders the board and removes the event listeners
         * return nothing
         */
         _onGameOver() {
            Board._removeEventListener();
             let helper = this;
             let timeout = window.setTimeout(function () {
                 helper._renderBoard();
                 window.clearTimeout(timeout);
                 Board._removeEventListener();
             }, 500);

        }

        /**
         * no params
         * empties the dom and then renders the board with width and height of the model
         * return nothing
         */
         _renderBoard() {
            this._dom.empty();
            let width = this._model.width;
            let height = this._model.height;
            // render rows
            for(let i=0; i<=height; i++){
               this._dom.prepend('<div class="row '+i+'"></div>');
            }
            // render cells
            let rows = $('#fancyGame>.board>.row');

            for(let i=0; i<rows.length; i++){
                for(let j=0; j<width; j++) {
                    // create string
                    let html = '<div class="cell float '+j+'">';
                    // get current symbol of position in model
                    if(i!==0){
                        let elem;
                        //if(this._model.field[i][j] !== undefined)
                            elem = this._model.field[i-1][j];
                        //else elem = null;
                        // set token in relation to symbol of position in model
                        // winning tokens
                        if(elem === 'x' && this._model._checkWinningTokens([i-1, j])) html += '<div class="ball finalBall winner player1">' +
                            '<div class="innerBall winner player1"/></div>';
                        else if(elem === 'o' && this._model._checkWinningTokens([i-1, j])) html += '<div class="ball finalBall winner player2">' +
                            '<div class="innerBall winner player2"/></div>';

                        else if (elem === 'x') html += '<div class="ball finalBall player1">' +
                            '<div class="innerBall player1"/></div>';
                        else if (elem === 'o') html += '<div class="ball finalBall player2">' +
                            '<div class="innerBall player2"/></div>';
                    }

                    html += '<div class="innerCell"/></div>';
                    // apppend string to row
                    $(rows[i]).append(html);
                }
            }
            // add border to board
            Board._addBorder();
            // add clickhandler
            this._clickhandler();
            this._tokenInteraction();
            console.log(this._model.toString());
        }

        /**
         * no params
         * adds a border to border-cells
         * return nothing
         */
        static _addBorder(){
            $('#fancyGame .board .row:nth-child(2)>div').addClass('border-top');
            $('#fancyGame .board .row>div:last-child').addClass('border-right');
            $('#fancyGame .board .row>div:first-child').addClass('border-left');
            $('#fancyGame .board .row:last-child>div').addClass('border-bottom');
        }

        /**
         * param columnIndex
         * if insert token is possible - create new ball, insert token and remove
         * token over the board so that it seems like it fell down
         * else show alert message that insert is not possible
         * return nothing
         */
        _insertToken(columnIndex){
            // if insert token is possible - insert token
            if (this._model.isInsertTokenPossibleAt(columnIndex)) {
                let currentPlayer = this._model.getCurrentPlayer();
                this._createNewBall(columnIndex, currentPlayer);
                this._model.insertTokenAt(columnIndex);
                $('.toDrop').remove();
            }
            // else show error message
            else{
                $('.popup-background').remove();
                this._dom.append(
                    "<div class='popup-background' style='display: none'>" +
                    "<div class='popup'>" +
                    "<p class='text' style='text-align: center'>" +
                    "As the selected column is already full you are <br/>not allowed to insert a token.</p>" +
                    "<div class='button-container'> " +
                    "<button id='cancel' class='button'><span>Okay</span></button>"+
                    "</div>"+
                    "</div>" +
                    "</div>"
                );
                $('.popup-background').fadeIn();
                // add click handler
                $('#cancel').on("click", (e) => {
                    console.log("cancel");
                    $('.popup-background').fadeOut();
                });
            }
        }

        /**
         * param col - column index where token should be inserted
         * gets current player from model for the color of the token
         * then gets the column of the given column index and checks
         * if the current position is undefined (no token there)
         * then creates a new ball which automatically falls down
         * return nothing
         */
        _createNewBall(col, currentPlayerModel){
            // get all cells
            let divs = $('#fancyGame .board .cell');
            // get current player
            let currentPlayer;
            if(currentPlayerModel) currentPlayer = 1;
            else currentPlayer = 2;
            // get position to insert new token
            for(let i=0; i<divs.length; i++){
                // get column
                if($(divs[i]).hasClass(col)){
                    for(let row=0; row<this._model.height; row++){
                        // create new token
                        if(this._model.field[this._model.height-1-row][col] == "-"){
                            new lib.Ball(
                                col*60+10,
                                0,
                                this._model.height-row+1,
                                currentPlayer
                            );
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }

    class Ball {
        /**
         * param left - left offset of the token
         * param top - top offset of the token
         * rows - row in which token should be inserted
         * currentplayer - for class name to set the right color
         * creates a new token
         * return nothing
         */
        constructor(left, top, rows, currentplayer) {
            // create string for token
            let $ball = $(
                '<div class=" dropping ball player'+currentplayer+'">' +
                '<div class="innerBall player'+currentplayer+'"/></div>'
            ).appendTo($('#fancyGame>.board'));
            // set position of the token
            $ball.css('left', left);
            $ball.css('top', top);
            // set variables
            let yMax = 60*rows-55 // position where token is landing
            let y = top; // start position
            let v = 0; // velocity starts at 0 (standing still)
            let a = 3.3; //
            // generate animation of movement
            const intervalId = setInterval(() => {
                v += a;
                y += v;
                if (y >= yMax) {
                    if (Math.abs(v) < 1) {
                        clearInterval(intervalId);
                    }
                    v *= -.3;
                    y = yMax;
                }
                $ball.css('top', y);
            }, 20)
        }
    }

    // export views
    lib.Board = Board;
    lib.Controls = Controls;
    lib.Restart = Restart;
    lib.Ball = Ball;
}());
