$(function () {
    let model = new lib.GameModel();
    let simple = $('.simpleGame');
    let start = $('#startDisplay');

    //let simpleView = new lib.SimpleView(model, simple);
    let startDisplay = new lib.StartDisplay(start, model);
    //simpleView._render();

    simple.hide();
    startDisplay._render();
    //simpleView._hide();


    // get dom elements
    let domControls = $('#fancyGame>.controls');
    let domBoard = $('#fancyGame>.board');
    let domRestart = $('#fancyGame>.restart-container');
    $('#fancyGame').hide();

    // create views
    let controlsView = new lib.Controls(model, domControls);
    let boardView = new lib.Board(model, domBoard);
    let restartView = new lib.Restart(model, domRestart);


    // add event listener to init complete
    //$(model).on(model.EVENTS.INIT_COMPLETE, _initGame);

    // init model
    //model.init();

    /**
     * no params
     * init all views
     * return nothing
     */
    function _initGame() {
        controlsView.init();
        boardView.init();
        restartView.init();

        console.log("init completed.");
    }
});
