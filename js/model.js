lib = window.lib || {};

(function () {
    class GameModel{
        /**
         * no params
         * constructor
         * return nothing
         */
        constructor(){
            this.nameP1 = "Player1";
            this.nameP2 = "Player2";
            this.player1 = "x";
            this.player2 = "o";
            this.width = 7;
            this.height = 6;
            this.againstComputer = false; // set, if the the player 1 plays against a computer
        }

        setSymbols(p1symbol, p2symbol){
            if(p1symbol === p2symbol){}
            else if(p1symbol === "") {}
            else if(p1symbol === "-") {}
            else if(p1symbol.length>1){}
            else this.player1 = p1symbol;

            if(p1symbol === p2symbol){}
            else if(p2symbol === "") {}
            else if(p2symbol === "-") {}
            else if(p2symbol.length>1){}
            else this.player2 = p2symbol;
        }

        setNames(namep1, namep2){
            if(namep1 !== "") this.nameP1 = namep1;
            if(namep2 !== "") this.nameP2 = namep2;
        }

        setField(cols, rows){
            if(isNaN(cols)){}
            else if(cols < 4) this.width = 4;
            else if(cols > 7) this.width = 7;
            else this.width = cols;

            if(isNaN(rows)){}
            else if(rows < 4) this.height = 4;
            else if(rows > 6) this.height = 6;
            else this.height = rows;
        }

        // getter for current player
        getCurrentPlayer(){
            return this.currentPlayer;
        }

        setAgainstComputer(value){
            this.againstComputer = value;
        }

        // getter for name of current player
        getCurrentPlayerName(){
            if(this.getCurrentPlayer()) return this.nameP1;
            else return this.nameP2;
        }

        // getter for symbol of current player
        getCurrentPlayerSymbol(){
            if(this.getCurrentPlayer()) return this.player1;
            else return this.player2;
        }

        adjustSettings(){
            console.log("adjust settings");
            $(this).trigger('settings');
        }

        /**
         * param columnIndex index of column where to insert token
         * inserts token at given index to the last empty place
         * then checks if current player won, if yes trigger event
         * if no checks if matrix is full, if yes trigger event
         * if no change current player and then trigger event
         * return nothing
         */
        insertTokenAt(columnIndex){
            let cnt = 1; // counter for number of stones in column
            // increase counter to number of stones in column
            for(let i=0; i<this.height; i++){
                if(this.field[this.height-cnt][columnIndex] !== this.empty){
                    cnt++;
                }
                else break;
            }
            // add stone at last empty place in column
            if(this.currentPlayer){
                this.field[this.height-cnt][columnIndex] = this.player1;
            }
            else{
                this.field[this.height-cnt][columnIndex] = this.player2;
            }

            // check if current player won
            if(this._checkIfPlayerWins(this.height-cnt, columnIndex, this.getCurrentPlayerSymbol())){
                // trigger event
                $(this).trigger('playerWon');
            }
            // if noone one won go on with program
            else{
                // check if full
                if(this._checkIfFull()){
                    // trigger event
                    $(this).trigger('gameOver');
                }
                // if not full go on with program
                else{
                    // change current player
                    this.currentPlayer = !this.currentPlayer;
                    //if player 1 plays against the computer, the method will be called
                    if(this.againstComputer === true && this.currentPlayer === false){
                        $(this).trigger('computersTurn');
                    }

                    // trigger event
                    $(this).trigger('change');
                }
            }
        }

        /**
         * param columnIndex index of column where to insert token
         * checks if cell at column index and highest row is empty
         * return true or false
         */
        isInsertTokenPossibleAt(columnIndex){
            console.log(columnIndex+1);
            if(this.gameOver === true){
                return false;
            } else if (this.field[0][columnIndex] === this.empty){
                return true;
            }
        }

        /**
         * no params
         * builds a string out of every field
         * return string
         */
        toString(){
            let string = '';
            // go through the whole field (rows and columns)
            for(let i=0; i<this.height; i++){
                for(let j=0; j<this.width; j++) {
                    // check if current token is not a winning token
                    if(!this._checkWinningTokens([i, j])){
                        string += this.field[i][j];
                    }
                    // if current token is a winning token - highlight with class
                    else{
                        string += '<span class="highlight">' + this.field[i][j] + '</span>';
                    }
                }
                string += '\n';
            }
            return string;
        }

        /**
         * no params
         * resets the whole game
         * return true or false
         */
        init(){
            // options to set to personalize game
            this.tokensToWin = 4;
            this.empty = "-";
            this.field = this._emptyField(); // empty field
            this.winningTokens =[]; // empty array of winning tokens
            this.currentPlayer = true; // set current player
            this.gameOver = false; // changes, if the game is over (player 1 or 2 wins, OR nobody)
            $(this).trigger('initComplete'); // trigger event
        }



        // ------------------------- private methods -------------------------- //

        /**
         * no params
         * this method calculates a random value, where the computer can set his token
         */
        _computerSetToken(){
            let min = 0;
            let max = this.width;

            let notPossible = true;

            while (notPossible){
                let randomInsertValue = Math.floor(Math.random() * (max - min + 1)) + min;

                if(this.isInsertTokenPossibleAt(randomInsertValue)){
                    $(this).trigger("tokenInsertedByComputer", [randomInsertValue]);
                    notPossible = false;
                }
            }
        }

        /**
         * param toFind
         * checks if token to find is in winningTokens array
         * return true or false
         */
        _checkWinningTokens(toFind){
            for(let i=0; i<this.winningTokens.length; i++){
                if(this.winningTokens[i][0] === toFind[0] &&
                    this.winningTokens[i][1] === toFind[1])
                    return true;
            }
            return false;
        }

        /**
         * param row y-coordinate of inserted token
         * param col x-coordinate of inserted token
         * param player symbol of current player
         * checks all directions starting at the inserted token
         * if any direction contains same or more than tokensToWin return true
         * return true or false
         */
        _checkIfPlayerWins(row, col, player) {
            // check cells in all directions
            let horizontal = this._checkCells(row, col, 1, 0, player);
            let vertical = this._checkCells(row, col, 0, 1, player);
            let diagonal1 = this._checkCells(row, col, 1, 1, player);
            let diagonal2 = this._checkCells(row, col, -1, 1, player);

            // check if any direction >= tokens to win
            return (horizontal || vertical || diagonal1 || diagonal2)
        }

        /**
         * param row y-coordinate of inserted token
         * param col x-coordinate of inserted token
         * param offsetX 0, 1 or -1 for x-direction
         * param offsetY 0, 1 or -1 for y-direction
         * param player symbol of current player
         * checks two opposite directions starting at the current token
         * and adds coordinates to winningTokens as long as they are
         * all the same in a row/column/diagonal
         * return nothing
         */
        _checkCells(row, col, offsetX, offsetY, player) {
            // check left and/or up until the end of the field
            for(let i=0; this.winningTokens.length<this.tokensToWin; i++){
                // go to the left for i*offsetX
                let x = col-i*offsetX;
                // go to the top for i*offsetY
                let y = row-i*offsetY;

                // check if still in field
                if(x<0 || y<0) break;

                // check if field to check has the symbol of current player
                if(this.field[y][x] === player){
                    // if yes, save coordinates
                    this.winningTokens.push([y, x]);
                }
                // if no, stop checking this direction
                else break;
            }

            // check right and/or down until the end of the field
            for(let i=1; this.winningTokens.length<this.tokensToWin; i++){
                // go to the right for i*offsetX
                let x = col+i*offsetX;
                // go to the bottom for i*offsetY
                let y = row+i*offsetY;

                // check if still in field
                if(x>=this.width || y>=this.height) break;

                // check if field to check has the symbol of current player
                if(this.field[y][x] === player){
                    // if yes, save coordinates
                    this.winningTokens.push([y, x]);
                }
                // if no, stop checking this direction
                else break;
            }

            // check if won
            if(this.winningTokens.length >= this.tokensToWin){
                this.gameOver = true;
                return true;
            }
            // if no, empty winningTokens and return false
            else this.winningTokens = [];
            return false;
        }

        /**
         * no params
         * checks the whole field (rows and colums)
         * if any cell is empty return false, else return true
         * return true or false
         */
        _checkIfFull(){
            for(let i=0; i<this.height; i++){
                for(let j=0; j<this.width; j++) {
                    if(this.field[i][j] === this.empty) return false;
                }
            }
            return true;
        }

        /**
         * no params
         * creates a new field matrix filled with empty symbols
         * return field
         */
        _emptyField(){
            let field = [];
            for(let i=0; i<this.height; i++){
                field[i] = [];
                for(let j=0; j<this.width; j++) {
                    field[i][j] = this.empty;
                }
            }
            return field;
        }
    }

    lib.GameModel = GameModel;
}());
